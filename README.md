# Volume Hardware Key Media Session Demo

Regarding Android application development, this is a quick demo for how to use a media session to access inputs of hardware keys (e.g. the volume buttons). This is really not a great idea because using a media session when you are not playing media will either interfere with the apps usage, or interfere with the user trying to play other media. That said, if you wanted to know how to do this, now you know!

Growth for this repository includes looking for an alternative way to use the media session to access whether the hardware buttons were pressed without interferring with the use playing music or other media.

## Set-up

Don't forget to add 

    implementation "com.android.support:support-compat:28.0.0"
    implementation "com.android.support:support-media-compat:28.0.0"
    
to your Module: App Gradle file so that you can access the Media libararies. 

## Compatability 

As of this posting this app works on a Google Pixel 1 running Android 9. 