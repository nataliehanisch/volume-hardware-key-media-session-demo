package space.lona.natalie.volumehardwarekeymediasessiondemo;

import android.content.Context;
import android.media.AudioManager;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

class VolumeKeyController {

    private Context context;
    private final AudioManager audio;

    VolumeKeyController(Context superContext) {

        context = superContext;
        audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        createMediaSession();

        MainActivity.getInstance().updateText("Media Session Started");

    }

    private void createMediaSession() {

        MediaSessionCompat mMediaSession = new MediaSessionCompat(context,"volume-control");
        mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PLAYING, 0, 0)
                .build());
        mMediaSession.setPlaybackToRemote(getVolumeProvider());
        mMediaSession.setActive(true);
    }

    private VolumeProviderCompat getVolumeProvider() {

        int STREAM_TYPE = AudioManager.STREAM_MUSIC;
        int currentVolume = audio.getStreamVolume(STREAM_TYPE);
        int maxVolume = audio.getStreamMaxVolume(STREAM_TYPE);
        final int VOLUME_UP = 1;
        final int VOLUME_DOWN = -1;

        return new VolumeProviderCompat(VolumeProviderCompat.VOLUME_CONTROL_RELATIVE, maxVolume, currentVolume) {

            private final int STREAM_TYPE = 0;

            @Override
            public void onAdjustVolume(int direction) {

                if (direction == VOLUME_UP) {

                    MainActivity.getInstance().updateText("Volume Up");

                    audio.adjustStreamVolume(STREAM_TYPE,
                            AudioManager.ADJUST_RAISE, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                }

                else if (direction == VOLUME_DOWN) {

                    MainActivity.getInstance().updateText("Volume Down");

                    audio.adjustStreamVolume(STREAM_TYPE,
                            AudioManager.ADJUST_LOWER, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                }

                setCurrentVolume(audio.getStreamVolume(STREAM_TYPE));
            }
        };
    }

}