package space.lona.natalie.volumehardwarekeymediasessiondemo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String message = "";
    private int count = 0;
    private static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        TextView textView = findViewById(R.id.textView);
        textView.setText(message);

        Context context = getApplicationContext();
        new VolumeKeyController(context);

    }

    public void updateText(String text) {

        message += "Event " + count + ":\t" + text + "\n";

        TextView textView = findViewById(R.id.textView);
        textView.setText(message);

        count += 1;

    }


    public static MainActivity getInstance() {
        return instance;
    }

}
